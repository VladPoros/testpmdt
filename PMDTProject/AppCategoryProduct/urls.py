from django.urls import path
from . import views

urlpatterns = [
    path('categories/', views.CategoryView.as_view()),
    path('category/<int:pk>/', views.TreeCategoryView.as_view()),
    path('card/<int:pk>/detail', views.CardView.as_view()),
    path('card/<int:pk>/products', views.ProductListView.as_view()),
]
