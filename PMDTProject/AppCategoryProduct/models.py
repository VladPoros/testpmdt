from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from .GetColor import get_color


class Category(MPTTModel):
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    parent = TreeForeignKey('self', null=True, default=None, related_name='children', on_delete=models.CASCADE,
                            blank=True)

    def save(self, *args, **kwargs):
        if self.parent.level == 4:
            raise ValueError('Top nesting reached!')
        super(Category, self).save(*args, **kwargs)

    class Meta:
        db_table = 'category'
        indexes = [models.Index(fields=['name'])]

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    area = models.CharField(max_length=255, unique=True, blank=True)
    diameter = models.FloatField(unique=True, blank=True)
    length = models.PositiveIntegerField(unique=True, blank=True)
    color = models.PositiveIntegerField(choices=get_color(), blank=True)
    image = models.ImageField(upload_to='images', blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'products'
        indexes = [models.Index(fields=['name'])]

    def __str__(self):
        return self.name
