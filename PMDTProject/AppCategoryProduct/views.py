from .models import Category, Product
from .serializers import CategorySerializer, TreeCategorySerializer, CardSerializer, ProductListSerializer
from rest_framework.response import Response
from rest_framework.views import APIView


class CategoryView(APIView):

    def get(self, request):
        categories = Category.objects.filter(parent_id=None)
        serializer = CategorySerializer(categories, many=True)
        return Response({"categories": serializer.data})


class TreeCategoryView(APIView):

    def get(self, request, pk):
        categories = Category.objects.filter(tree_id=pk)
        serializer = TreeCategorySerializer(categories, many=True)
        return Response({"category, section, group, subgroup, card": serializer.data})


class CardView(APIView):

    def get(self, request, pk):
        product = Product.objects.filter(category_id=pk)
        serializer = CardSerializer(product, many=True)
        return Response({"card of product": serializer.data})

class ProductListView(APIView):

    def get(self, request, pk):
        product = Product.objects.filter(category_id=pk)
        serializer = ProductListSerializer(product, many=True)
        return Response({"product of card": serializer.data})

