from django.core.management.base import BaseCommand
from faker import Faker
from AppCategoryProduct.models import Category, Product
from progress.bar import Bar

class Command(BaseCommand):
    help = 'Add new card'

    def add_arguments(self, parser):
        parser.add_argument('-c', '--count.category', type=int, default=50)
        parser.add_argument('-p', '--count.products', type=int, default=100)

    def __create_category(self, count):

        fake = Faker()

        bar = Bar('Processing generate products', max=count)

        for _ in range(count):
            category = Category()
            category.name = fake.word()
            category.description = fake.sentence()
            category.save()
        bar.finish()


    def __create_products(self, count):

        fake = Faker()

        bar = Bar('Processing generate products', max=count)


        for _ in range(count):
            product = Product()
            product.name = fake.word()
            product.description = fake.sentence()
            product.length = fake.random.randint(1, 100000)
            product.diameter = fake.random.randint(1, 100000)
            product.area = fake.lexify(letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ')
            product.color = fake.random.randint(1, 5)
            product.image = 'images/CALLIGRAFUTURISM – 10'
            product.save()
            bar.next()
        bar.finish()
        self.stdout.write(self.style.SUCCESS(f"Successfully generate products"))



    def handle(self, *args, **options):
        self.stdout.write('===================')
        self.stdout.write(self.style.SUCCESS("Start seed database"))
        self.stdout.write('===================')

        self.__create_category(options['count.category'])
        self.__create_products(options['count.products'])
