from django.apps import AppConfig


class AppcategoryproductConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AppCategoryProduct'
