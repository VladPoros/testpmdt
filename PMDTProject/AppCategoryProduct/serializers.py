from .models import Category, Product
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['name', 'description', 'id']


class TreeCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['name', 'id']


class CardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['name', 'area', 'diameter', 'length', 'color', 'image']


class ProductListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['name']
